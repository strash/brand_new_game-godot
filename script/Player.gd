extends Creature


export var jump_impulse = 800

# направление
func get_direction():
	return Vector2(
		Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left"),
		-1 if Input.is_action_just_pressed("ui_up") and is_on_floor() else 1
	)

# перемещение игрока
func get_move_velocity(linear_velocity, direction, speed, is_jump_interrupt, is_move_interrupt):
	var result = linear_velocity
	result.x = direction.x * speed.x
	result.y += get_physics_process_delta_time() * gravity
	if direction.y == -1:
		result.y = direction.y * speed.y
	if is_jump_interrupt:
		result.y = 0
	if is_move_interrupt:
		result.x = 0
	return result

# отскок от моба
func get_jump_impulse(linear_velocity, impulse):
	var result = linear_velocity
	result.y = -impulse
	return result

# сигнал при столкновении с мобом
func _on_EnemyDetector_area_entered(area):
	velocity = get_jump_impulse(velocity, jump_impulse)

# смерть игрока
func _on_EnemyDetector_body_entered(body):
	queue_free()


func _physics_process(delta):
	var direction = get_direction()
	var is_jump_interrupt = Input.is_action_just_released("ui_up") and velocity.y < 0
	var is_move_interrupt = Input.is_action_just_released("ui_right") or Input.is_action_just_released("ui_left")
	velocity = get_move_velocity(velocity, direction, speed, is_jump_interrupt, is_move_interrupt)
	velocity = move_and_slide(velocity, FLOOR_NORMAL)
