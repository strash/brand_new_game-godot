extends "res://script/Creature.gd"


# сигнал обнаружения упавшего игрока на голову моба
func _on_PlayerDetector_body_entered(body):
	if body.global_position.y > get_node("PlayerDetector").global_position.y:
		return
	get_node("CollisionShape2D").disabled = true
	queue_free()


func _physics_process(delta):
	if is_on_wall():
		velocity.x *= -1
	velocity.y = move_and_slide(velocity, FLOOR_NORMAL).y


func _ready():
	set_physics_process(false)
	velocity.x = -speed.x
