extends KinematicBody2D
class_name Creature

# warning-ignore:unused_class_variable
var velocity = Vector2.ZERO
# warning-ignore:unused_class_variable
export var speed = Vector2(200, 40000)
# warning-ignore:unused_class_variable
export var gravity = 4000
const FLOOR_NORMAL = Vector2.UP